package io.gitlab.kakaruu.example.chat.ws;

import io.gitlab.kakaruu.example.chat.ws.handler.ChatSocketHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@RequiredArgsConstructor
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
  // @Autowired가 필요 없네? 싱기
  private final ChatSocketHandler chatHandler;

  @Override
  public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    // 되도록이면 다른 페이지 경로와 겹치지 않게 /ws 경로 아래로 설정하자
    // /ws/chat이라는 경로로 websocket을 연결할 수 있도록 지정한다.
    // setAllowedOrigins(*)으로 모든 Origin에 대해 접근을 허락한다.(cors 설정이랑 따로인가?)
    registry.addHandler(chatHandler, "/ws/chat").setAllowedOrigins("*");
  }
}