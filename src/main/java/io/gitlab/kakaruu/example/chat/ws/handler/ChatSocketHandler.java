package io.gitlab.kakaruu.example.chat.ws.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.ArrayList;
import java.util.List;

@Component
@Log4j2
public class ChatSocketHandler extends TextWebSocketHandler {
  // 연결된 세션들
  private static List<WebSocketSession> sessions = new ArrayList<>();

  @Override
  protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
    String payload = message.getPayload(); // 받은 메시지
    log.info("받은 메시지: " + payload);

    // 연결된 모든 세션들에게 메시지를 전달한다.
    for(WebSocketSession s: sessions) {
      s.sendMessage(message);
    }
  }

  // 새로운 연결이 시작되면 호출되는 메소드
  @Override
  public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    log.info("세션 시작됨: " + session);

    sessions.add(session);
  }

  // 연결이 종료되면 호출되는 메소드
  @Override
  public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    log.info("세션 종료됨: " + session);
    sessions.remove(session);
  }
}
