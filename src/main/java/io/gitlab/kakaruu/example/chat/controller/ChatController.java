package io.gitlab.kakaruu.example.chat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ChatController {
  @RequestMapping("/chat")
  public String process() {
    return "chat/chat";
  }
}
